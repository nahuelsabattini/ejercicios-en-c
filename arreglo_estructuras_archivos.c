#include <stdio.h>
#include <stdlib.h>


typedef struct {

	char nombre[20];
	char apellido[20];
	float nota1; 
	float nota2;
	int dni;

}Alumnos;

int posicion(int n){
	return (n-1);
};

int main(){

	int c, pos=0;

	printf("\nIngrese la cantidad de alumnos a registrar: ");
	scanf("%d",&c);

	Alumnos alumno[c];

	FILE *archivo = fopen("alumnos.bd","w+");


	if(archivo==NULL){
		printf("\nERROR");
		exit(EXIT_FAILURE);
	}

	for(int i=0; i<c; i++){

		printf("\nIngrese el nombre del alumno %d: ",i+1);
		getchar();
		fgets(alumno[i].nombre,20,stdin);

		printf("\nIngrese el apellido del alumno %d: ",i+1);
		fgets(alumno[i].apellido,20,stdin);

		printf("\nIngrese la nota 1 del alumno %d: ",i+1);
		scanf("%f",&alumno[i].nota1);

		printf("\nIngrese la nota 2 del alumno %d: ",i+1);
		scanf("%f",&alumno[i].nota2);

		printf("\nIngrese el DNI del alumno %d: ",i+1);
		scanf("%d",&alumno[i].dni);

		fwrite(&alumno,1,sizeof(Alumnos),archivo);
	}

	printf("\nDATOS DE LOS ALUMNOS\n");

	for(int j=0; j<c;j++){

		pos = posicion(j) * sizeof(Alumnos);
		fseek(archivo,pos,SEEK_SET);

		fread(&alumno,1,sizeof(Alumnos),archivo);

		printf("\nDATOS DEL ALUMNO %d",j+1);

		printf("\nNOMBRE: %s",alumno[j].nombre);
		printf("\nAPELLIDO: %s",alumno[j].apellido);
		printf("\nNOTA 1: %f",alumno[j].nota1);
		printf("\nNOTA 2: %f",alumno[j].nota2);
		printf("\nDNI: %d",alumno[j].dni);
	}

	fclose(archivo);



	return 0;
}

