#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void menu(int *a1, int *b1);
void sumar(int a1, int b1);
void restar(int a1, int b1);
void multiplicar(int a1, int b1);
void dividir(int a1, int b1);
void elevar_numeros(int a1, int b1);



void menu(int *a1, int *b1){

	int op, valor1,valor2;

	valor1 = *a1;
	valor2 = *b1;
	printf("\n---BIEVENIDIDO---\n");

	printf("1) Sumar numeros: \n");
	printf("2) Restar numeros: \n");
	printf("3) Multiplicar numeros: \n");
	printf("4) Dividir numeros: \n");
	printf("5) Elevar numeros: \n");
	printf("6) Salir\n");

	printf("\nIngrese una opcion: ");
	scanf("%d",&op);

	switch(op){

		case 1: sumar(valor1,valor2); break;
		case 2: restar(valor1,valor2);break;
		case 3: multiplicar(valor1,valor2);break;
		case 4: dividir(valor1,valor2);break;
		case 5: elevar_numeros(valor1,valor2); break; 
		case 6: 
			printf("-----HASTA LUEGO-----\n");
			exit(1);
			break;
	}

}

void sumar(int a1, int b1){
	printf("\nLa suma de los numeros es: %d", a1+b1);
	menu(&a1,&b1);
}

void restar(int a1, int b1){
	printf("\nLa resta de los numeros es: %d", a1-b1);
	menu(&a1,&b1);
}

void multiplicar(int a1, int b1){

	printf("\nLa multiplicacion de los nuemros es: %d",a1 * b1);
	menu(&a1,&b1);
}

void dividir(int a1, int b1){
	int aux;

	if(b1 == 0){
		printf("\nERROR EN LA DIVISION, NO ES POSIBLE DIVIDIR EN 0.");
		printf("\nPor favor ingrese un nuevo valor distinto de 0: ");
		scanf("%d",&aux);
		b1 = aux;
		printf("\nLa division entre %d y %d es: %d ",a1,b1,a1/b1);
	}else{
		printf("\nLa division entre los numeros es: %d",a1/b1);
	}
	menu(&a1,&b1);
}

void elevar_numeros(int a1, int b1){
    
    int v1,v2;
    
    v1 = pow(a1,2);
    v2 = pow(b1,2);

	printf("\nEl numero %d elevado al cuadrado es: %d", a1, v1);
	printf("\nEL numero %d elevado al cuadrado es: %d", b1, v2);
	menu(&a1,&b1);
}


int main()
{
	
	int a, b;
	char opcion;

	printf("\nIngrese un valor: ");
	scanf("%d",&a);

	printf("\nIngrese un segundo valor: ");
	scanf("%d",&b);
	

    printf("\nDesea visualizar un menu con las operaciones disponibles sobre los valores ingresados? (y/n): ");
    getchar();
	scanf("%c",&opcion);

	if(opcion == 'y'){
	    menu(&a,&b);
	}else{
	    printf("Los numeros ingresados fueron: %d y %d\n",a,b);
	    printf("---HASTA LUEGO---");
    }
	
	return 0;
}