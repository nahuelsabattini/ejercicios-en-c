/**
1.- Programar una función que lea desde el stream de entrada estándar, un
número entero que será la longitud del radio de una circunferencia
a) controle la entrada a valores menores que 100
b) defina a PI como una macro
c) calcule la longitud de la circunferencia (2 x PI x radio)
d) calcule el área del círculo (PI x radio al cuadrado)
e) envíe los valores calculados al stream de salida estándar.
→ 4 puntos
f) (6 puntos extra) controle el ingreso para que solo permita ingreso de enteros.
**/


#include <stdio.h>
#include <math.h>
#define PI 3.14

float radio_circunferencia(){

	float radio;
	int aux;

	do{
	printf("\nIngrese el radio de la circunferencia: ");
	scanf("%f",&radio);
	aux = radio;
	
	if(radio>100){printf("El radio debe ser menor que 100");}

	
	while(radio - aux !=0){
	printf("\nError-Solo de aceptan numeros enteros.");
	printf("\nIngrese el radio de la circunferencia: ");
	getchar();
	scanf("%f",&radio);
	aux = radio;
	

	}
	
	
	}while(radio>100);

	return radio;

};


int main(){

	int radio;
	radio = radio_circunferencia();

	printf("\nEl area del circulo es: %3.2f",PI*pow(radio,2));
	printf("\nLa longitud de la circunferencia es: %3.2f",2*PI*radio);



	return 0;
}