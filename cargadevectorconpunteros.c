/**

1) Crear un programa que tenga como variable local un arreglo que tenga n números enteros. El programa
tendrá tres funciones, a saber:
a) Una primera función que cargara todos los elementos del vector
b) Una función que en su código mostrará en pantalla todos los elementos del vector, y
permanecerá a la espera de que se oprima una tecla.
c) Una función que calculará el promedio de todos los elementos y devolverá a la función main
el promedio de los mismos que la función main mostrará en pantalla.

**/

#include <stdio.h>
#include <time.h>
#define N 5


void cargaVector(float *v){
	printf("\t----INICIANDO CARGA DEL VECTOR---- \n\nPOR FAVOR ESPERE...\n");
	sleep(5);

	for (int i = 0; i < N; ++i)
	{
		printf("\nIngrese el [%d]° valor del vector: ",i+1);
		scanf("%f",&v[i]);
	}

	printf("\n----VECTOR CARGADO CON EXITO!----\n");

};



void imprimirVector(float *v){

	printf("\nPresione una tecla para continuar...");
	getchar();
	getchar();

	for (int i = 0; i < N; ++i)
	{
		printf("\nValor -[%d]- = %2.2f \n",i+1,v[i]);
	}
};

float promedioVector(float *v){

	float sumador=0; 

	for (int i = 0; i < N; ++i)
	{
		sumador+=v[i];	
	}

	return sumador/N;
};

int main(){

	float vector[N];
	float promedio;
	float *v;

	v = vector;

	cargaVector(vector);
	imprimirVector(vector);

	promedio = promedioVector(vector);

	printf("\nEL promedio de los elementos del vector es: %2.2f\n",promedio);
	
	return 0;
}



