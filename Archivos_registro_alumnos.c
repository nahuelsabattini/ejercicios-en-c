#include <stdio.h>
#include <stdlib.h>


typedef struct{

	char nombre[20];
	char apellido[20];
	float nota1; 
	float nota2;
	int key; 
}Alumno;


int posicion(int key_estructura){
	return sizeof(Alumno)*(key_estructura-1);
};


int registrar_alumno(FILE *archivo, int cantidad_alumnos){

	Alumno alumno; 

	int posicion_alumno; 

	alumno.key = ++cantidad_alumnos;
	printf("\nIngrese el nombre del alumno: "); getchar();fgets(alumno.nombre,20,stdin);
	printf("\nIngrese el apellido del alumno: ");fgets(alumno.apellido,20,stdin);
	printf("\nIngrese la nota 1 del alumno:"); scanf("%f",&alumno.nota1);
	printf("\nIngrese la nota 2 del alumno: "); scanf("%f",&alumno.nota2);

	posicion_alumno=posicion(alumno.key);
	fseek(archivo,posicion_alumno,SEEK_SET);
	fwrite(&alumno,1,sizeof(Alumno),archivo);
	printf("\nEl alumno con Key: [%d] fue registrado correctamente.",alumno.key);
	getchar();

	return cantidad_alumnos;
};

void modificar_datos(FILE *archivo, int cantidad_alumnos){

	Alumno alumno; 
	int key,posicion_alumno;

	printf("\nIngrese el Key del alumno que desea modificar: ");scanf("%d",&key);

	if(key <=  0 || key > cantidad_alumnos){
		printf("\nError...");
	}else{

		posicion_alumno = posicion(key);
		fseek(archivo,posicion_alumno,SEEK_SET);
		fread(&alumno,1,sizeof(Alumno),archivo);

		printf("\nAlumno encontrado - Datos del alumno ");
		printf("\nNombre: %s",alumno.nombre);
		printf("\nApellido: %s",alumno.apellido);
		printf("\nNota 1: %f",alumno.nota1);
		printf("\nNota 2: %f",alumno.nota2);

		printf("\nPor favor ingrese los nuevo datos...");
		printf("\nNuevo nombre: "); fgets(alumno.nombre,20,stdin);
		printf("\nNuevo apellido: "); fgets(alumno.apellido,20,stdin);
		printf("\nNueva nota 1: "); scanf("%f",&alumno.nota1);
		printf("\nNueva nota 2: "); scanf("%f",&alumno.nota2);

		fseek(archivo,posicion_alumno,SEEK_SET);
		fwrite(&alumno,1,sizeof(Alumno),archivo);
		printf("\nEL CAMBIO EN LOS DATOS FUE REGISTRADO CON EXITO!...Presiones una tecla");
		getchar();
	}
};


void mostar_alumnos(FILE *archivo, int cantidad_alumnos){

	Alumno alumno;

	int posicion_alumno, i=1;

	while(i<cantidad_alumnos+1){

		posicion_alumno = posicion(i);
		fseek(archivo,posicion_alumno,SEEK_SET);
		fread(&alumno,1,sizeof(Alumno),archivo);

		if(alumno.key == -1){
			printf("\nRegistro no encontrado: este alumno fue eliminado.");
		}else{
			printf("\n----------------Datos del alumno [%d]-------------",i);
			printf("\nNombre: %s",alumno.nombre);
			printf("\nApellido: %s",alumno.apellido);
			printf("\nNota 1: %f ",alumno.nota1);
			printf("\nNota 2: %f ",alumno.nota2);

		}
		i++;
	}



};


void elimiar_alumno(FILE *archivo, int cantidad_alumnos){

	Alumno alumno;
	int key, posicion_alumno;

	printf("\nIngrese el Key del alumno a eliminar: "); scanf("%d",&key);

	if(key <=0 || key>cantidad_alumnos){
		printf("\nKey incorrecto..."); getchar();
	}else{

		posicion_alumno = posicion(key);
		fseek(archivo,posicion_alumno,SEEK_SET);
		alumno.key = -1;
		fwrite(&alumno,1,sizeof(Alumno),archivo);
		printf("\nAlumno eliminado con exito.");

	}

};

 

int main(){

	FILE *archivo;
	int cantidad_alumnos=0, opcion;

	archivo = fopen("alumnos.bd","w+b");

	while(1){

			puts("\n\t\t\t---SISTEMA DE REGISRO DE ALUMNOS---\n");
			puts("\n1) Cargar alumno.");
			puts("\n2) Modificar datos de un alumno.");
			puts("\n3) Eliminar alumno. ");
			puts("\n4) Imprimir alumnos.");
			puts("\n5) Salir.");

			puts("\nIngrese una opcion: "); scanf("%d",&opcion);

			switch(opcion){

				case 1: cantidad_alumnos = registrar_alumno(archivo,cantidad_alumnos); break;
				case 2: modificar_datos(archivo, cantidad_alumnos); break;
				case 3: elimiar_alumno(archivo, cantidad_alumnos); break;
				case 4: mostar_alumnos(archivo,cantidad_alumnos); break;
				case 5: puts("\nSaliendo del sistema..."); exit(1); break;
				default: puts("\nOpcion incorrecta");

			}
			puts("\nPresione una tecla para continuar...");getchar();
			system("clear");



	}

	fclose(archivo);

	return 0;

}